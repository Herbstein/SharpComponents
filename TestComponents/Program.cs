﻿namespace TestComponents {
    using System;
    using System.Diagnostics;
    using System.IO;

    using SharpComponents.Components;
    using SharpComponents.Enums;

    internal class Program {
        private static void Main(string[] args) {
            var document = new Document();
            document.Styling.FontFamily = FontFamily.PalatinoLinotype;
            document.Styling.Color = Color.CornflowerBlue;
            document.Styling.BackgroundColor = Color.Beige;

            var title = new Title("Hello, World!");
            document.Head.Add(title);

            var p = new Paragraph("Hello paragraph!");
            //p.Styling.Color = Color.Blue;
            document.Body.Add(p);

            var h = new Header("Hello, Headers!", HeaderSize.Four);
            //h.Styling.Color = Color.Gray;
            document.Body.Add(h);

            var a = new A(new Uri("https://gitlab.com"), "GitLab");
            //a.Styling.Color = Color.Red;
            document.Body.Add(a);

            File.WriteAllText("index.html", document.ToString());
            Process.Start("index.html");
        }
    }
}
