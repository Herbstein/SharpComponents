﻿namespace SharpComponents.Enums {
    /// <summary>
    ///     Enumerates every color useable in CSS
    /// </summary>
    public enum Color {
        /// <summary>
        ///     Alice Blue
        /// </summary>
        AliceBlue = 0xf0f8ff,

        /// <summary>
        ///     Antique White
        /// </summary>
        AntiqueWhite = 0xfaebd7,

        /// <summary>
        ///     Aqua
        /// </summary>
        Aqua = 0x00ffff,

        /// <summary>
        ///     Aqua Marine
        /// </summary>
        Aquamarine = 0x7fffd4,

        /// <summary>
        ///     Azure
        /// </summary>
        Azure = 0xf0ffff,

        /// <summary>
        ///     Beige
        /// </summary>
        Beige = 0xf5f5dc,

        /// <summary>
        ///     Bisque
        /// </summary>
        Bisque = 0xffe4c4,

        /// <summary>
        ///     Black
        /// </summary>
        Black = 0x000000,

        /// <summary>
        ///     Blanched Almond
        /// </summary>
        BlanchedAlmond = 0xffebcd,

        /// <summary>
        ///     Blue
        /// </summary>
        Blue = 0x0000ff,

        /// <summary>
        ///     Blue Violet
        /// </summary>
        BlueViolet = 0x8a2be2,

        /// <summary>
        ///     Brown
        /// </summary>
        Brown = 0xa52a2a,

        /// <summary>
        ///     Burly Wood
        /// </summary>
        BurlyWood = 0xdeb887,

        /// <summary>
        ///     Cadet Blue
        /// </summary>
        CadetBlue = 0x5f9ea0,

        /// <summary>
        ///     Chartreuse
        /// </summary>
        Chartreuse = 0x7fff00,

        /// <summary>
        ///     Chocolate
        /// </summary>
        Chocolate = 0xd2691e,

        /// <summary>
        ///     Coral
        /// </summary>
        Coral = 0xff7f50,

        /// <summary>
        ///     Cornflower Blue
        /// </summary>
        CornflowerBlue = 0x6495ed,

        /// <summary>
        ///     Cornsilk
        /// </summary>
        Cornsilk = 0xfff8dc,

        /// <summary>
        ///     Crimson
        /// </summary>
        Crimson = 0xdc143c,

        /// <summary>
        ///     Cyan
        /// </summary>
        Cyan = 0x00ffff,

        /// <summary>
        ///     Dark Blue
        /// </summary>
        DarkBlue = 0x00008b,

        /// <summary>
        ///     Dark Cyan
        /// </summary>
        DarkCyan = 0x008b8b,

        /// <summary>
        ///     Dark Golden Rod
        /// </summary>
        DarkGoldenRod = 0xb8860b,

        /// <summary>
        ///     Dark Gray
        /// </summary>
        DarkGray = 0xa9a9a9,

        /// <summary>
        ///     Dark Green
        /// </summary>
        DarkGreen = 0x06400,

        /// <summary>
        ///     Dark Khaki
        /// </summary>
        DarkKhaki = 0xbdb76b,

        /// <summary>
        ///     Dark Magenta
        /// </summary>
        DarkMagenta = 0x8b008b,

        /// <summary>
        ///     Dark Olive Green
        /// </summary>
        DarkOliveGreen = 0x556b2f,

        /// <summary>
        ///     Dark Orange
        /// </summary>
        DarkOrange = 0xff8c00,

        /// <summary>
        ///     Dark Orchid
        /// </summary>
        DarkOrchid = 0x9932cc,

        /// <summary>
        ///     Dark Red
        /// </summary>
        DarkRed = 0x8b0000,

        /// <summary>
        ///     Dark Salmon
        /// </summary>
        DarkSalmon = 0xe9967a,

        /// <summary>
        ///     Dark Sea Green
        /// </summary>
        DarkSeaGreen = 0x8fbc8f,

        /// <summary>
        ///     Dark Slate Blue
        /// </summary>
        DarkSlateBlue = 0x483d8b,

        /// <summary>
        ///     Dark Slate Gray
        /// </summary>
        DarkSlateGray = 0x2f4f4f,

        /// <summary>
        ///     Dark Slate Grey
        /// </summary>
        DarkSlateGrey = 0x2f4f4f,

        /// <summary>
        ///     Dark Turquoise
        /// </summary>
        DarkTurquoise = 0x00ced1,

        /// <summary>
        ///     Dark Violet
        /// </summary>
        DarkViolet = 0x9400d3,

        /// <summary>
        ///     Deep Pink
        /// </summary>
        DeepPink = 0xff1493,

        /// <summary>
        ///     Deep Sky Blue
        /// </summary>
        DeepSkyBlue = 0xbfff,

        /// <summary>
        ///     Dim Gray
        /// </summary>
        DimGray = 0x696969,

        /// <summary>
        ///     Dim Grey
        /// </summary>
        DimGrey = 0x696969,

        /// <summary>
        ///     Dodger Blue
        /// </summary>
        DodgerBlue = 0x1e90ff,

        /// <summary>
        ///     Fire Brick
        /// </summary>
        FireBrick = 0xb22222,

        /// <summary>
        ///     Floral White
        /// </summary>
        FloralWhite = 0xfffaf0,

        /// <summary>
        ///     Forest Green
        /// </summary>
        ForestGreen = 0x228b22,

        /// <summary>
        ///     Fuchsia
        /// </summary>
        Fuchsia = 0xff00ff,

        /// <summary>
        ///     Gainsboro
        /// </summary>
        Gainsboro = 0xdcdcdc,

        /// <summary>
        ///     Ghost White
        /// </summary>
        GhostWhite = 0xf8f8ff,

        /// <summary>
        ///     Gold
        /// </summary>
        Gold = 0xffd700,

        /// <summary>
        ///     Golden Rod
        /// </summary>
        GoldenRod = 0xdaa520,

        /// <summary>
        ///     Gray
        /// </summary>
        Gray = 0x808080,

        /// <summary>
        ///     Grey
        /// </summary>
        Grey = 0x808080,

        /// <summary>
        ///     Green
        /// </summary>
        Green = 0x008000,

        /// <summary>
        ///     Green Yellow
        /// </summary>
        GreenYellow = 0xadff2f,

        /// <summary>
        ///     Honeydew
        /// </summary>
        HoneyDew = 0xf0fff0,

        /// <summary>
        ///     Hot Pink
        /// </summary>
        HotPink = 0xff69b4,

        /// <summary>
        ///     Indian Red
        /// </summary>
        IndianRed = 0xcd5c5c,

        /// <summary>
        ///     Indigo
        /// </summary>
        Indigo = 0x4b0082,

        /// <summary>
        ///     Ivory
        /// </summary>
        Ivory = 0xfffff0,

        /// <summary>
        ///     Khaki
        /// </summary>
        Khaki = 0xf0e68c,

        /// <summary>
        ///     Lavender
        /// </summary>
        Lavender = 0xe6e6fa,

        /// <summary>
        ///     Lavender Blush
        /// </summary>
        LavenderBlush = 0xfff0f5,

        /// <summary>
        ///     Lawn Green
        /// </summary>
        LawnGreen = 0x7cfc00,

        /// <summary>
        ///     Lemon Chiffon
        /// </summary>
        LemonChiffon = 0xfffacd,

        /// <summary>
        ///     Light Blue
        /// </summary>
        LightBlue = 0xadd8e6,

        /// <summary>
        ///     Light Coral
        /// </summary>
        LightCoral = 0xf08080,

        /// <summary>
        ///     Light Cyan
        /// </summary>
        LightCyan = 0xe0ffff,

        /// <summary>
        ///     Light Golden Rod Yellow
        /// </summary>
        LightGoldenRodYellow = 0xfafad2,

        /// <summary>
        ///     Light Gray
        /// </summary>
        LightGray = 0xd3d3d3,

        /// <summary>
        ///     Light Grey
        /// </summary>
        LightGrey = 0xd3d3d3,

        /// <summary>
        ///     Light Green
        /// </summary>
        LightGreen = 0x90ee90,

        /// <summary>
        ///     Light Pink
        /// </summary>
        LightPink = 0xffb6c1,

        /// <summary>
        ///     Light Salmon
        /// </summary>
        LightSalmon = 0xffa07a,

        /// <summary>
        ///     Light Sea Green
        /// </summary>
        LightSeaGreen = 0x20b2aa,

        /// <summary>
        ///     Light Sky Blue
        /// </summary>
        LightSkyBlue = 0x87cefa,

        /// <summary>
        ///     Light Slate Gray
        /// </summary>
        LightSlateGray = 0x778899,

        /// <summary>
        ///     Light Slate Grey
        /// </summary>
        LightSlateGrey = 0x778899,

        /// <summary>
        ///     Light Steel Blue
        /// </summary>
        LightSteelBlue = 0xb0c4de,

        /// <summary>
        ///     Light Yellow
        /// </summary>
        LightYellow = 0xffffe0,

        /// <summary>
        ///     Lime
        /// </summary>
        Lime = 0x00ff00,

        /// <summary>
        ///     Lime Green
        /// </summary>
        LimeGreen = 0x32cd32,

        /// <summary>
        ///     Linen
        /// </summary>
        Linen = 0xfaf0e6,

        /// <summary>
        ///     Magenta
        /// </summary>
        Magenta = 0xff00ff,

        /// <summary>
        ///     Maroon
        /// </summary>
        Maroon = 0x800000,

        /// <summary>
        ///     Medium Aqua Marine
        /// </summary>
        MediumAquaMarine = 0x66cdaa,

        /// <summary>
        ///     Medium Blue
        /// </summary>
        MediumBlue = 0x0000cd,

        /// <summary>
        ///     Medium Orchid
        /// </summary>
        MediumOrchid = 0xba55d3,

        /// <summary>
        ///     Medium Purple
        /// </summary>
        MediumPurple = 0x9370db,

        /// <summary>
        ///     Medium Sea Green
        /// </summary>
        MediumSeaGreen = 0x3cb371,

        /// <summary>
        ///     Medium Slate Green
        /// </summary>
        MediumSlateBlue = 0x3cb371,

        /// <summary>
        ///     Medium Spring Green
        /// </summary>
        MediumSpringGreen = 0x00fa9a,

        /// <summary>
        ///     Medium Turquoise
        /// </summary>
        MediumTurquoise = 0x48d1cc,

        /// <summary>
        ///     Medium Violet Red
        /// </summary>
        MediumVioletRed = 0xc71585,

        /// <summary>
        ///     Midnight Blue
        /// </summary>
        MidnightBlue = 0x191970,

        /// <summary>
        ///     Mint Cream
        /// </summary>
        MintCream = 0xf5fffa,

        /// <summary>
        ///     Misty Rose
        /// </summary>
        MistyRose = 0xffe4e1,

        /// <summary>
        ///     Moccasin
        /// </summary>
        Moccasin = 0xffe4b5,

        /// <summary>
        ///     Navajo White
        /// </summary>
        NavajoWhite = 0xffdead,

        /// <summary>
        ///     Navy
        /// </summary>
        Navy = 0x000080,

        /// <summary>
        ///     Old Lace
        /// </summary>
        OldLace = 0xfdf5e6,

        /// <summary>
        ///     Olive
        /// </summary>
        Olive = 0x808000,

        /// <summary>
        ///     Olive Drab
        /// </summary>
        OliveDrab = 0x6b8e23,

        /// <summary>
        ///     Orange
        /// </summary>
        Orange = 0xffa500,

        /// <summary>
        ///     Orange Red
        /// </summary>
        OrangeRed = 0xff4500,

        /// <summary>
        ///     Orchid
        /// </summary>
        Orchid = 0xda70d6,

        /// <summary>
        ///     Pale Golden Red
        /// </summary>
        PaleGoldenRod = 0xeee8aa,

        /// <summary>
        ///     Pale Green
        /// </summary>
        PaleGreen = 0x98fb98,

        /// <summary>
        ///     Pale Turquoise
        /// </summary>
        PaleTurquoise = 0xafeeee,

        /// <summary>
        ///     Pale Violet Red
        /// </summary>
        PaleVioletRed = 0xdb7093,

        /// <summary>
        ///     Papaya Whip
        /// </summary>
        PapayaWhip = 0xffefd5,

        /// <summary>
        ///     Peach Puff
        /// </summary>
        PeachPuff = 0xffdab9,

        /// <summary>
        ///     Peru
        /// </summary>
        Peru = 0xcd8535f,

        /// <summary>
        ///     Pink
        /// </summary>
        Pink = 0xffc0cb,

        /// <summary>
        ///     Plum
        /// </summary>
        Plum = 0xdda0dd,

        /// <summary>
        ///     Powder Blue
        /// </summary>
        PowderBlue = 0xb0e0e6,

        /// <summary>
        ///     Purple
        /// </summary>
        Purple = 0x800080,

        /// <summary>
        ///     Rebecca Purple
        /// </summary>
        RebeccaPurple = 0x663399,

        /// <summary>
        ///     Red
        /// </summary>
        Red = 0xff0000,

        /// <summary>
        ///     Rosy Brown
        /// </summary>
        RosyBrown = 0xbc8f8f,

        /// <summary>
        ///     Royal Blue
        /// </summary>
        RoyalBlue = 0x4169e1,

        /// <summary>
        ///     Saddle Brown
        /// </summary>
        SaddleBrown = 0x8b4513,

        /// <summary>
        ///     Salmon
        /// </summary>
        Salmon = 0xfa8072,

        /// <summary>
        ///     Sandy Brown
        /// </summary>
        SandyBrown = 0xf4a460,

        /// <summary>
        ///     Sea green
        /// </summary>
        SeaGreen = 0x2e8b57,

        /// <summary>
        ///     Sea Shell
        /// </summary>
        SeaShell = 0xfff5ee,

        /// <summary>
        ///     Sienna
        /// </summary>
        Sienna = 0xa0522d,

        /// <summary>
        ///     Silver
        /// </summary>
        Silver = 0xc0c0c0,

        /// <summary>
        ///     Sky Blue
        /// </summary>
        SkyBlue = 0x87ceeb,

        /// <summary>
        ///     Slate Blue
        /// </summary>
        SlateBlue = 0x6a5acd,

        /// <summary>
        ///     Slate Gray
        /// </summary>
        SlateGray = 0x708090,

        /// <summary>
        ///     Slate Grey
        /// </summary>
        SlateGrey = 0x708090,

        /// <summary>
        ///     Snow
        /// </summary>
        Snow = 0xfffafa,

        /// <summary>
        ///     Spring Green
        /// </summary>
        SpringGreen = 0x00ff7f,

        /// <summary>
        ///     Steel Blue
        /// </summary>
        SteelBlue = 0x4682b4,

        /// <summary>
        ///     Tan
        /// </summary>
        Tan = 0xd2b48c,

        /// <summary>
        ///     Teal
        /// </summary>
        Teal = 0x008080,

        /// <summary>
        ///     Thistle
        /// </summary>
        Thistle = 0xd8bfd8,

        /// <summary>
        ///     Tomato
        /// </summary>
        Tomato = 0xff6347,

        /// <summary>
        ///     Turquoise
        /// </summary>
        Turquoise = 0x40e0d0,

        /// <summary>
        ///     Violet
        /// </summary>
        Violet = 0xee82ee,

        /// <summary>
        ///     Wheat
        /// </summary>
        Wheat = 0xf5deb3,

        /// <summary>
        ///     White
        /// </summary>
        White = 0xffffff,

        /// <summary>
        ///     White Smoke
        /// </summary>
        WhiteSmoke = 0xf5f5f5,

        /// <summary>
        ///     Yellow
        /// </summary>
        Yellow = 0xffff00,

        /// <summary>
        ///     Yellow Green
        /// </summary>
        YellowGreen = 0x9acd32
    }

    /// <summary>
    ///     Extensions to the <see cref="Color" /> enum.
    /// </summary>
    public static class ColorExtensions {
        /// <summary>
        ///     Turn the <see cref="Color" /> into a hex format.
        /// </summary>
        /// <param name="color">The color to turn into hex.</param>
        /// <returns>A string containing the hex value with a '#' prepended.</returns>
        public static string ToHex(this Color color) => $"#{(int)color:X6}";
    }
}
