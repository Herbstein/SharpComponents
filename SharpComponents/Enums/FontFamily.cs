﻿// ReSharper disable InconsistentNaming
#pragma warning disable 1591

namespace SharpComponents.Enums {
    using System;
    using System.Linq;
    using System.Text;

    /// <summary>
    ///     Enumeration of all web-safe fonts.
    /// </summary>
    [Flags]
    public enum FontFamily : ulong {
        Georgia = 0x1,
        PalatinoLinotype = 0x2,
        TimesNewRoman = 0x4,
        BookAntiqua = 0x8,
        Serif = 0x10,
        Times = 0x20,
        Arial = 0x40,
        Helvetica = 0x80,
        SansSerif = 0x100,
        ArialBlack = 0x200,
        Gadget = 0x400,
        ComicSansMS = 0x800,
        Cursive = 0x1000,
        Impact = 0x2000,
        Charcoal = 0x4000,
        LucidaSansUnicode = 0x8000,
        LucidaGrande = 0x10000,
        Tahoma = 0x20000,
        Geneva = 0x40000,
        TrebuchetMS = 0x80000,
        Verdana = 0x100000,
        CourierNew = 0x200000,
        Courier = 0x400000,
        Monospace = 0x800000,
        LucidaConsole = 0x1000000,
        Monaco = 0x2000000
    }

    public static class FontFamilyExtensions {
        public static string ToHTMLString(this FontFamily fontFamily) {
            var builder = new StringBuilder("");

            if (fontFamily.HasFlag(FontFamily.Georgia)) {
                builder.Append($"Georgia {(fontFamily&~FontFamily.Georgia).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.PalatinoLinotype)) {
                builder.Append($"\'Palatino Linotype\' {(fontFamily&~FontFamily.PalatinoLinotype).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.TimesNewRoman)) {
                builder.Append($"\'Times New Roman\' {(fontFamily&~FontFamily.TimesNewRoman).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.BookAntiqua)) {
                builder.Append($"\'Book Antiqua\' {(fontFamily&~FontFamily.BookAntiqua).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.Serif)) {
                builder.Append($"serif {(fontFamily&~FontFamily.Serif).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.Times)) {
                builder.Append($"Times {(fontFamily&~FontFamily.Times).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.Arial)) {
                builder.Append($"Arial {(fontFamily&~FontFamily.Arial).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.Helvetica)) {
                builder.Append($"Helvetica {(fontFamily&~FontFamily.Serif).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.SansSerif)) {
                builder.Append($"sans-serif {(fontFamily&~FontFamily.SansSerif).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.ArialBlack)) {
                builder.Append($"\'Arial Black\' {(fontFamily&~FontFamily.ArialBlack).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.Gadget)) {
                builder.Append($"Gadget {(fontFamily&~FontFamily.Gadget).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.ComicSansMS)) {
                builder.Append($"\'Comic Sans MS\' {(fontFamily&~FontFamily.ComicSansMS).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.Cursive)) {
                builder.Append($"Cursive {(fontFamily&~FontFamily.Cursive).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.Impact)) {
                builder.Append($"Impact {(fontFamily&~FontFamily.Impact).ToHTMLString()} ");
            } else if (fontFamily.HasFlag(FontFamily.Charcoal)) {
                builder.Append($"Charcoal {(fontFamily&~FontFamily.Charcoal).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.LucidaSansUnicode)) {
                builder.Append($"\'Lucida Sans Unicode\' {(fontFamily&~FontFamily.LucidaSansUnicode).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.LucidaGrande)) {
                builder.Append($"\'Lucida Grande\' {(fontFamily&~FontFamily.LucidaGrande).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.Tahoma)) {
                builder.Append($"Tahoma {(fontFamily&~FontFamily.Tahoma).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.Geneva)) {
                builder.Append($"Geneva {(fontFamily&~FontFamily.Geneva).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.TrebuchetMS)) {
                builder.Append($"\'Trebuchet MS\' {(fontFamily&~FontFamily.TrebuchetMS).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.Verdana)) {
                builder.Append($"Verdana {(fontFamily&~FontFamily.Verdana).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.CourierNew)) {
                builder.Append($"\'Courier New\' {(fontFamily&~FontFamily.CourierNew).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.Courier)) {
                builder.Append($"Courier {(fontFamily&~FontFamily.Courier).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.Monospace)) {
                builder.Append($"monospace {(fontFamily&~FontFamily.Monospace).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.LucidaConsole)) {
                builder.Append($"\'Lucida Console\' {(fontFamily&~FontFamily.LucidaConsole).ToHTMLString()}");
            } else if (fontFamily.HasFlag(FontFamily.Monaco)) {
                builder.Append($"Monaco {(fontFamily|~FontFamily.Monaco).ToHTMLString()}");
            }

            return builder.ToString();
        }
    }
}
