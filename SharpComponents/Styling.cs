﻿namespace SharpComponents {
    using System.Collections.Generic;
    using System.Text;

    using SharpComponents.Enums;

    /// <summary>
    ///     Contains all styling that can be applied to a <see cref="HtmlComponent" />
    /// </summary>
    public class Styling {
        private readonly Dictionary<string, object> stylings = new Dictionary<string, object>();

        /// <summary>
        ///     Enumerate the styling to inline html style
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            if (stylings.Count == 0) {
                return string.Empty;
            }

            var builder = new StringBuilder();

            foreach (var styling in stylings) {
                builder.Append($"{styling.Key}:");
                switch (styling.Key)
                {
                    case "color":
                        builder.Append($"{GetColorString()}");
                        break;
                    case "background-color":
                        builder.Append($"{GetBackgroundColorString()}");
                        break;
                    case "font-family":
                        builder.Append($"{GetFontFamilyString()}");
                        break;
                }

                builder.Append(";");
            }

            return builder.ToString();
        }

        #region Color

        /// <summary>
        ///     Get or Set the color style
        /// </summary>
        public Color Color {
            get { return (Color)stylings["color"]; }
            set { stylings["color"] = value; }
        }

        private string GetColorString() => $"#{(int)stylings["color"]:X6}";

        #endregion Color

        #region BackgroundColor

        /// <summary>
        ///     Get or Set the background-color style
        /// </summary>
        public Color BackgroundColor {
            get { return (Color)stylings["background-color"]; }
            set { stylings["background-color"] = value; }
        }

        private string GetBackgroundColorString() => $"#{(int)stylings["background-color"]:X6}";

        #endregion

        #region FontFamily

        public FontFamily FontFamily {
            get {
                return (FontFamily)stylings["font-family"];
            }
            set {
                stylings["font-family"] = value;
            }
        }

        private string GetFontFamilyString() {
            var fontFamilyString = FontFamily.ToHTMLString();
            return fontFamilyString.Remove(fontFamilyString.Length - 1);
        }

        #endregion
    }
}
