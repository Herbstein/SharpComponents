﻿namespace SharpComponents {
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    ///     Represents an arbitrary HTML component
    /// </summary>
    public class HtmlComponent {
        /// <summary>
        ///     The object containing the styling information for the current tag.
        /// </summary>
        public readonly Styling Styling = new Styling();

        /// <summary>
        ///     The nested components of the current component.
        /// </summary>
        protected readonly List<HtmlComponent> Children = new List<HtmlComponent>();

        private readonly Dictionary<string, object> properties = new Dictionary<string, object>();
        private readonly string tagName;

        /// <summary>
        ///     Initializes a HtmlComponent of the specified tag.
        /// </summary>
        /// <param name="tag">The tag used for this component.</param>
        public HtmlComponent(string tag) {
            tagName = tag;
        }

        /// <summary>
        ///     Initialize a HtmlComponent with the specified tag and children.
        /// </summary>
        /// <param name="tag">The tag used for this component.</param>
        /// <param name="children">The nested children of this component-</param>
        public HtmlComponent(string tag, params HtmlComponent[] children) : this(tag) {
            foreach (var component in children) {
                Children.Add(component);
            }
        }

        /// <summary>
        ///     Initialize a HtmlComponent with the specified tag, content, and children.
        /// </summary>
        /// <param name="tag">The tag used for this component.</param>
        /// <param name="content">The content used within the tags.</param>
        /// <param name="index">The index of the content amongs the children. 0 by default.</param>
        /// <param name="children">The nested children of this component.</param>
        public HtmlComponent(string tag, string content, int index = 0, params HtmlComponent[] children) : this(tag, children) {
            Content = content;
            ContentIndex = index;
        }

        /// <summary>
        ///     The content of the tag.
        /// </summary>
        public string Content { get; set; } = string.Empty;

        /// <summary>
        ///     The index of the current tags content amongst the nested components.
        /// </summary>
        public int ContentIndex { get; set; }

        /// <summary>
        ///     Add a child to this component.
        /// </summary>
        /// <param name="child"></param>
        public void Add(HtmlComponent child) {
            Children.Add(child);
        }

        /// <summary>
        ///     Add a list of children to this component.
        /// </summary>
        /// <param name="newChildren"></param>
        public void Add(IEnumerable<HtmlComponent> newChildren) {
            foreach (var child in newChildren) {
                Children.Add(child);
            }
        }

        /// <summary>
        ///     Adds a property to the component
        /// </summary>
        /// <param name="key">The string identifier of the property.</param>
        /// <param name="value">The value of the property.</param>
        public void AddProperty(string key, object value) {
            properties.Add(key, value);
        }

        /// <summary>
        ///     Checks if the specified property exists.
        /// </summary>
        /// <param name="key">The key to check for.</param>
        /// <returns>Whether the key is contained in the component.</returns>
        public bool ContainsProperty(string key) {
            return properties.ContainsKey(key);
        }

        /// <summary>
        ///     Modifies the given property to a new value
        /// </summary>
        /// <param name="key">The key of the property to modify</param>
        /// <param name="value">The new styling value</param>
        public void ModifyProperty(string key, object value) {
            properties[key] = value;
        }

        /// <summary>
        ///     Reads, and returns, the specified property
        /// </summary>
        /// <param name="key">The property to check for.</param>
        /// <returns>The object assigned to the property</returns>
        public object ReadProperty(string key) {
            return properties[key];
        }

        /// <summary>
        ///     Removes a property from the component
        /// </summary>
        /// <param name="key">The key to remove fromt he component</param>
        /// <returns>The value of the removed property</returns>
        public object RemoveProperty(string key) {
            var obj = properties[key];

            properties.Remove(key);

            return obj;
        }

        /// <summary>
        ///     Converts the HTML component and its children to a string.
        /// </summary>
        /// <returns>A string containing the HTML for this component and it's children.</returns>
        public override string ToString() {
            // Run the pre-render processes.
            PreRenderProcess();

            var builder = new StringBuilder();

            // Append opening tag
            builder.Append($"<{tagName}");

            // Add all properties to the HTML
            if (properties.Count > 0) {
                foreach (var property in properties) {
                    builder.Append($" {property.Key}={property.Value}");
                }
            }

            // Add styling property if one or more styles are set.
            var styling = Styling.ToString();
            if (styling != string.Empty) {
                builder.Append($" style=\"{styling}\"");
            }

            // Close opening tag
            builder.Append(">\n");

            // Append content if it is coming before all nested components.
            if ((ContentIndex == 0) && (Content != string.Empty)) {
                builder.Append($"{Content}\n");
            }

            // Append all nested components
            for (var index = 0; index < Children.Count; index++) {
                // Inject content if the correct index has been reached
                if ((ContentIndex != 0) && (index == ContentIndex) && (Content != string.Empty)) {
                    builder.Append($"{Content}\n");
                }

                // Append the child as HTML
                builder.Append($"{Children[index]}\n");
            }

            // Append closing tag
            builder.Append($"</{tagName}>");

            // Run post-render processes
            PostRenderProcess();

            return builder.ToString();
        }

        /// <summary>
        ///     Override this to do cleanup for special tags after automatic rendering
        /// </summary>
        protected virtual void PostRenderProcess() { }

        /// <summary>
        ///     Override this to prepare special tags for automatic rendering
        /// </summary>
        protected virtual void PreRenderProcess() { }
    }
}
