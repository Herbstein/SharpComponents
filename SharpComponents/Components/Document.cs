﻿namespace SharpComponents.Components {
    using System.Collections.Generic;

    /// <summary>
    ///     Represents the building block of the HTML document. Supports adding to both the head and the body.
    /// </summary>
    public class Document : HtmlComponent {
        /// <summary>
        ///     Initializes an empty HTML worksheet
        /// </summary>
        public Document() : base("html") { }

        /// <summary>
        ///     Gets the document head.
        /// </summary>
        public Head Head { get; } = new Head();

        /// <summary>
        ///     Gets the document body.
        /// </summary>
        public Body Body { get; } = new Body();

        /// <summary>
        ///     Append a component to the body
        /// </summary>
        /// <param name="component">The component to add</param>
        public void AppendBody(HtmlComponent component) {
            Head.Add(component);
        }

        /// <summary>
        ///     Appends a list of components to the body
        /// </summary>
        /// <param name="components">The list of components to append</param>
        public void AppendBody(IEnumerable<HtmlComponent> components) {
            Head.Add(components);
        }

        /// <summary>
        ///     Append a component to the head
        /// </summary>
        /// <param name="component">The component to add</param>
        public void AppendHead(HtmlComponent component) {
            Body.Add(component);
        }

        /// <summary>
        ///     Appends a list of components to the head
        /// </summary>
        /// <param name="components">The list of components to append</param>
        public void AppendHead(IEnumerable<HtmlComponent> components) {
            Body.Add(components);
        }

        /// <summary>
        ///     Remove Head and Body components from the child list. This makes sure no duplicates are added.
        /// </summary>
        protected override void PostRenderProcess() {
            Children.Remove(Head);
            Children.Remove(Body);
        }

        /// <summary>
        ///     Add Head and Body components to the child list
        /// </summary>
        protected override void PreRenderProcess() {
            Children.Add(Head);
            Children.Add(Body);
        }
    }
}
