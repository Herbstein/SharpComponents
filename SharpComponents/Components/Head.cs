﻿namespace SharpComponents.Components {
    /// <summary>
    ///     Represents the document head tag.
    /// </summary>
    public class Head : HtmlComponent {
        /// <summary>
        ///     Initialize a Head component.
        /// </summary>
        public Head() : base("head") { }

        #region Properties

        /// <summary>
        ///     Gets or sets the nested title tag's content.
        /// </summary>
        public string Title {
            get {
                foreach (var component in Children) {
                    if (component is Title) {
                        return component.Content;
                    }
                }

                return string.Empty;
            }
            set {
                foreach (var component in Children) {
                    if (component is Title) {
                        component.Content = value;
                        return;
                    }
                }

                Add(new Title(value));
            }
        }

        #endregion
    }
}
