﻿namespace SharpComponents.Components {
    /// <summary>
    ///     Represents the hx HTML tag
    /// </summary>
    public class Header : HtmlComponent {
        /// <summary>
        ///     Initialize a header with the size of 1
        /// </summary>
        /// <param name="content">The content of the header</param>
        public Header(string content = "") : base("h1", content) { }

        /// <summary>
        ///     Initializes the Header with the desired content and size
        /// </summary>
        /// <param name="content">The content of the header</param>
        /// <param name="size">The size of the header</param>
        public Header(string content, HeaderSize size) : base($"h{(int)size}", content) { }
    }

    /// <summary>
    ///     Indentifies the desired size of the header.
    /// </summary>
    /// <remarks>Can safely be turned into a integer without additional conversions.</remarks>
    public enum HeaderSize {
        /// <summary>
        ///     Represents the header size of 1
        /// </summary>
        One = 1,

        /// <summary>
        ///     Represents the header size of 2
        /// </summary>
        Two,

        /// <summary>
        ///     Represents the header size of 3
        /// </summary>
        Three,

        /// <summary>
        ///     Represents the header size of 4
        /// </summary>
        Four,

        /// <summary>
        ///     Represents the header size of 5
        /// </summary>
        Five,

        /// <summary>
        ///     Represents the header size of 6
        /// </summary>
        Six
    }
}
