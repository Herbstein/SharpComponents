﻿namespace SharpComponents.Components {
    /// <summary>
    ///     Represents the document body tag.
    /// </summary>
    public class Body : HtmlComponent {
        /// <summary>
        ///     Initialize a body component.
        /// </summary>
        public Body() : base("body") { }
    }
}
