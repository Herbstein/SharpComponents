﻿namespace SharpComponents.Components {
    /// <summary>
    ///     Represents the title HTML component
    /// </summary>
    public class Title : HtmlComponent {
        /// <summary>
        ///     Initialize the title component with a specified title.
        /// </summary>
        /// <param name="title">The title of the website.</param>
        public Title(string title) : base("title", title) { }
    }
}
