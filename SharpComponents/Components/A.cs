﻿namespace SharpComponents.Components {
    using System;

    /// <summary>
    ///     Represents the 'a' HTML tag
    /// </summary>
    public class A : HtmlComponent {
        /// <summary>
        ///     Initialize an 'a' tag with the specified content
        /// </summary>
        /// <param name="content">The content of the component</param>
        public A(string content = "") : base("a", content) { }

        /// <summary>
        ///     Initialize an 'a' tag with the given <see cref="Uri" /> as both the text and the href.
        /// </summary>
        /// <param name="uri">The href and content of the tag</param>
        /// <param name="content">The content of the component</param>
        public A(Uri uri, string content = "") : this(uri, content == string.Empty ? uri.ToString() : content, 0) { }

        /// <summary>
        ///     Initialize an 'a' tag with the given <see cref="Uri" />, and custom content.
        /// </summary>
        /// <param name="uri">The href of the tag</param>
        /// <param name="content">The content showed by the link</param>
        /// <param name="index">The index of the content string between the nested components</param>
        /// <param name="components">The nested components of the tag</param>
        public A(Uri uri, string content, int index = 0, params HtmlComponent[] components) : base("a", content, index, components) {
            Href = uri;
        }

        /// <summary>
        ///     Get or set the href property of the 'a' tag.
        /// </summary>
        public Uri Href {
            get { return (Uri)ReadProperty("href"); }
            set {
                if (ContainsProperty("href")) {
                    ModifyProperty("href", value);
                } else {
                    AddProperty("href", value);
                }
            }
        }
    }
}
