﻿namespace SharpComponents.Components {
    /// <summary>
    ///     Represents the p HTML tag
    /// </summary>
    public class Paragraph : HtmlComponent {
        /// <summary>
        ///     Initializes a paragraph with the specified
        /// </summary>
        /// <param name="content"></param>
        public Paragraph(string content) : base("p", content) { }
    }
}
